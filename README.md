Founded by Tracie Ellis in 2000, Aura Home has since become a premier designer bedlinen and homewares brand throughout Australia and abroad. Designed in Melbourne and inspired by Tracies love of global and Australian travel, we design and create a range of rich, textured pieces that bring contemporary style and luxury to any home. Striving for quality and elegance in everything we do, the Aura Home brand has become synonymous with sublime colours, tactile natural fabrics and exquisite patterns.

Website: https://www.aurahome.com.au/
